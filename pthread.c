#include <pthread.h>

/* glibc bug? Segfault on wrong pid.           */
/* Is known to U. Drapper, but won't be fixed. */
int main(void)
{
	pthread_kill(-1);
	return 0;
}

