#!/usr/bin/perl

# a quick-n-dirty cmdline pager for fefes blog
# navigate with 'n', 'p' and 'q'
# startup options: --color
# by Daniel Borkmann, borkmann@gnumaniacs.org

use strict;
use warnings;
use utf8;
use Encode;
use XML::RSS;
use LWP::UserAgent;
use Term::ANSIColor;
use Term::Size;
require HTML::TreeBuilder;
require HTML::FormatText;

my $color = 0;
my $with_colors = 0;
my ($columns, $rows) = Term::Size::chars *STDOUT{IO};
my $agent = LWP::UserAgent->new;
my $parser = XML::RSS->new;
my $formatter = HTML::FormatText->new( leftmargin => 0
				     , rightmargin => $columns - 2 );
my $rawrss = HTTP::Request->new(GET => 'http://blog.fefe.de/rss.xml?html');
my @colors = ( 'red'
	     , 'green'
	     , 'yellow'
	     , 'blue'
	     , 'magenta'
	     , 'cyan'
	     , 'white' );
my @links;
my $max;

$with_colors = 1 if (defined($ARGV[0]) && $ARGV[0] eq "--color");
$agent->default_header( 'Accept-Language' => 'en-US'
		      , 'Accept-Charset' => 'utf-8'
		      , 'Accept' => '*/*' );
$agent->agent('Fnord-Pager 1.0');
$agent->timeout(10);
$agent->env_proxy;
$parser->parse($agent->request($rawrss)->content);

sub cite
{
	my $ref = scalar(@links);
	my $link = shift;
	$link = "http://blog.fefe.de".$link if ($link =~ m/^\/\?ts=/);
	push(@links, $link);
	return "$ref";
}

open(TTY, "+</dev/tty") or die "No tty: $!";
system "stty cbreak </dev/tty >/dev/tty 2>&1";

$max = scalar(@{$parser->{'items'}});
for (my $elem = 0; $elem < $max; )
{
	my ($cmd, $html, $count, $lsize);
	my $item = $parser->{'items'}[$elem];
	my $content = $item->{'description'};

	system "clear";

	@links = ();

	$content =~ s/^\s+//;
	$content =~ s/\s+$//;
	$content =~ s/<a\s*href=\"(.*?)\">(.*?)<\/a>/"$2 \[".cite($1)."\]"/eg;

	$lsize = scalar(@links);
	$content = $content."\n<ul>";
	foreach ($count = 0; $count < $lsize; $count++)
	{
		$content = $content."<li>\[$count\] ".shift(@links)."</li>";
	}
	$content = $content."</ul>";
	$content = "<html><body>".$content."</body></html>";

	$html = HTML::TreeBuilder->new_from_content($content);
	$content = $formatter->format($html);
	$content = encode('utf-8', $content);

	if ($with_colors == 1) {
		print colored ['bold '.$colors[$color].' on_black'], "\r".$content;
		print colored ['reset'], "\n\n";
	} else {
		print "\r".$content."\n\n";
	}

	$color = ($color + 1) % scalar(@colors);

	$cmd = getc(TTY);

	if ($cmd eq 'q') {
		last;
	} elsif ($cmd eq 'n') {
		$elem = $elem + 1;
		$elem = $max - 1 if ($elem == $max);
	}elsif ($cmd eq 'p') {
		$elem = $elem - 1;
		$elem = 0 if ($elem < 0);
	}
}

system "stty -cbreak </dev/tty >/dev/tty 2>&1";
close(TTY);

exit 0;
